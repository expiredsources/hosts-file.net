# HpHosts

These hosts files are the original from the old HpHosts project, but in a 
updated version, where outdated records have been removed.

Do to the fact this is a git, you can travers through the commits, to locate 
the files from how they looked at the first commit.

The first commit where alle the files was imported in a unaltered version, 
can be found in [32776d88cf61c1e93d280d44d52b64ea53edad0e](https://bitbucket.org/expiredsources/hosts-file.net/commits/32776d88cf61c1e93d280d44d52b64ea53edad0e).

| File (current) | Original File name  | Contents                                                                 | License             |
|:---------------|:--------------------|:-------------------------------------------------------------------------|:--------------------|
| ats.txt        | hpHosts-ATS         | Ad/tracking servers.                                                     | Freeware            |
| emd.txt        | hpHosts-EMD         | Malware sites.                                                           | Freeware            |
| exp.txt        | hpHosts-EXP         | Exploit sites.                                                           | Freeware            |
| fsa.txt        | hpHosts-FSA         | Fraud sites.                                                             | Freeware            |
| grm.txt        | hpHosts-GRM         | Sites involved in spam                                                   | Freeware            |
| hjk.txt        | hpHosts-HJK         | Hijack sites.                                                            | Freeware            |
| mmt.txt        | hpHosts-MMT         | Sites involved in misleading marketing (e.g. fake Flash update adverts). | Freeware            |
| psh.txt        | hpHosts-PSH         | Phishing sites.                                                          | Freeware            |
| pup.txt        | hpHosts-PUP         | Block PUP (Potentially Unwanted Programs).                               | Freeware            |
| ---            | ~~hpHosts-HFS~~     | Spamming the hpHosts forums.                                             | ~~Freeware~~ noWare |
| ---            | ~~hpHosts~~         | MAIN, BIG Hosts file with almost `725 000` entries.                      | ~~Freeware~~ noWare |
| ---            | ~~hpHosts.partial~~ | Hosts were added to hpHosts AFTER the last full release.                 | ~~Freeware~~ noWare |
